import { type FC } from "react";
import members from "../data/members.json";
import submissions from "../data/submissions.json";
import Member from "./Member";

const Submissions: FC = () => {
  return (
    <div className="flex flex-row flex-wrap items-center justify-center py-10">
      {members.map((member) => (
        <Member key={member.id} data={member} selected={member.id === "238728915647070209"} faded={true}/>
      ))}
    </div>
  );
};

export default Submissions;
