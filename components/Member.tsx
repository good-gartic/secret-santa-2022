/* eslint-disable @next/next/no-img-element */
import { FC } from "react";
import { MemberData } from "../types";

type MemberProps = {
  data: MemberData;
  selected: boolean;
  faded: boolean;
  onMouseEnter?: () => void;
  onMouseLeave?: () => void;
  onClick?: () => void;
};

const Member: FC<MemberProps> = ({
  data,
  selected,
  faded,
  onMouseEnter,
  onMouseLeave,
  onClick,
}: MemberProps) => {
  return (
    <div
      className={`flex flex-row items-center justify-center p-4 m-4 bg-white rounded-full cursor-pointer transition-all hover:opacity-100 hover:shadow-xl ${
        selected ? "ring-4 ring-sky-700" : faded ? "opacity-" : ""
      }`}
      onMouseEnter={() => onMouseEnter && onMouseEnter()}
      onMouseLeave={() => onMouseLeave && onMouseLeave()}
      onClick={() => onClick && onClick()}
    >
      <img
        src={data.avatar}
        alt={`${data.username}'s avatar`}
        className="w-8 h-8 rounded-full shadow-lg"
      />
      <div className="flex flex-col items-start mx-4">
        <div className="text-sky-700 font-bold">{data.nickname}</div>
        {data.nickname !== data.username && (
          <div className="text-xs text-neutral-500">{data.username}</div>
        )}
      </div>
    </div>
  );
};

export default Member;
