export type MemberData = {
  id: string;
  avatar: string;
  username: string;
  nickname: string;
};

export type MemberSubmission = {
  from: string;
  to: string;
  image: string;
  text: string;
};
