import Head from "next/head";
import Submissions from "../components/Submissions";

export default function Home() {
  return (
    <>
      <Head>
        <title>Secret Santa 2022</title>
        <meta name="description" content="Good Gartic's Secret Santa 2022" />
        <meta name="og:title" content="Secret Santa 2022" />
        <meta name="og:description" content="Good Gartic's Secret Santa 2022" />
        <meta name="og:image" content="/logo.png" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>

      <main className="flex flex-col items-center justify-center min-h-screen bg-sky-200">
        <h2 className="text-3xl font-black text-sky-500">Good Gartic{<>&apos;</>}s</h2>
        <h1 className="text-7xl font-black text-sky-700">Secret Santa 2022</h1>

        <Submissions/>
      </main>
    </>
  );
}
